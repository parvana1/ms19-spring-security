package az.ingress.springbootsecurity.repository;

import az.ingress.springbootsecurity.model.Authority;
import az.ingress.springbootsecurity.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority,Long> {
Authority findByRole(Role role);
}
