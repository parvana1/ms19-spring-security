package az.ingress.springbootsecurity.exception;


import lombok.extern.slf4j.Slf4j;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import java.time.OffsetDateTime;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(UserNotFoundException.class)
    public final ResponseEntity<ErrorResponseDTO> handle(UserNotFoundException ex, WebRequest request){
        log.error("User not found:  {}");
        final ErrorResponseDTO response = ErrorResponseDTO
                .builder()
                .message("Not found.")
                .detail("User with id {} not found")
                .code("MS19-ERROR-001")
                .status(404)
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .timestamp(OffsetDateTime.now())
                .build();
        return ResponseEntity.status(404).body(response);

    }




}
