package az.ingress.springbootsecurity.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.http.HttpMethod.POST;

@Configuration
public class SecurityConfig {
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
      http.csrf().disable();
      http.cors().disable();
       // http.authorizeHttpRequests((auth)->auth.requestMatchers("/public").permitAll());
       // http.authorizeHttpRequests((auth)->auth.requestMatchers("/user").hasAnyRole("USER","ADMIN"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/user").permitAll());
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/test/public").permitAll());
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/test/user").hasAnyAuthority("USER","ADMIN"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/test/admin").hasAnyAuthority("ADMIN"));
        http.authorizeHttpRequests((auth)->auth.anyRequest().authenticated());
        http.httpBasic(Customizer.withDefaults());
        return http.build();
    }

//    @Bean
//    public UserDetailsService users(){
//        UserDetails user= User.builder()
//                .username("user")
//                .password("{bcrypt}$2a$10$PssRiERNK/LtXw96nZxS0O8PA.VNMvZhsh0EKcat3tUXeyCzW2djK")
//                .roles("USER")
//                .build();
//        UserDetails admin= User.builder()
//                .username("admin")
//                .password("{bcrypt}$2a$10$PssRiERNK/LtXw96nZxS0O8PA.VNMvZhsh0EKcat3tUXeyCzW2djK")
//                .roles("ADMIN","USER")
//                .build();
//        return new InMemoryUserDetailsManager(user,admin);
//    }

    @Bean
    public PasswordEncoder encoder(){
        return new BCryptPasswordEncoder();
    }
}
