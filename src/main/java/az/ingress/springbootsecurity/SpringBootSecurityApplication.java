package az.ingress.springbootsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSecurityApplication {

	public static void main(String[] args) {
		try{
			SpringApplication.run(SpringBootSecurityApplication.class, args);
		}
		catch (Exception e){
			e.printStackTrace();
		}

	}

}
