package az.ingress.springbootsecurity.model;

public enum Role {
    USER,
    ADMIN
}
