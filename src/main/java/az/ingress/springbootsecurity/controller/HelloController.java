package az.ingress.springbootsecurity.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class HelloController {
    @GetMapping("/public")
    public String sayPublicHello(){
        return "Public Hello";
    }
    //user&admin
    @GetMapping("/user")
    public String sayHello(){
        return "User & admin Hello";
    }
    @GetMapping("/admin")
    public String sayAdminHello(){
        return "Admin Hello";
    }
}
