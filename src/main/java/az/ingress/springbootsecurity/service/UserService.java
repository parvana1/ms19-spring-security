package az.ingress.springbootsecurity.service;

import az.ingress.springbootsecurity.dto.UserDto;
import az.ingress.springbootsecurity.model.Authority;
import az.ingress.springbootsecurity.model.Role;
import az.ingress.springbootsecurity.model.User;
import az.ingress.springbootsecurity.repository.AuthorityRepository;
import az.ingress.springbootsecurity.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
    private  final AuthorityRepository authorityRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    public void create(UserDto dto){
        Optional<User> byUsername=userRepository.findByUsername(dto.getUsername());
        if(byUsername.isPresent()){
            System.out.println("user var");
        }else{
            System.out.println(dto.getUsername());
            System.out.println(dto.getPassword());
            Authority userAuthority=authorityRepository.findByRole(Role.USER);
            System.out.println(userAuthority.toString());
            User user=User.builder()
                    .username(dto.getUsername())
                    .password(encoder.encode(dto.getPassword()))
                    .authorities(List.of(userAuthority))
                    .build();
            userRepository.save(user);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       return userRepository.findByUsername(username).orElseThrow(()-> new RuntimeException(" username not found"));
    }
}
